FROM node:stretch


RUN apt update && apt upgrade -y && apt install apt-transport-https; \
    echo "deb https://dl.bintray.com/sobolevn/deb git-secret main" | \
    tee -a /etc/apt/sources.list && wget -qO - \
    https://api.bintray.com/users/sobolevn/keys/gpg/public.key | \
    apt-key add - && apt update && apt install -y git-secret

RUN for i in gnupg ssh; do mkdir -m 700 -p ~/.$i/; done
COPY gpg.conf ~/.gnupg/gpg-agent.conf

ADD deployment/bin/*.sh /usr/bin/
